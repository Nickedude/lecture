--Övning 1 7/9 2015

--Assignment 1, The Maximum Function
maxi :: Integer -> Integer -> Integer
maxi x y = if x >= y then x else y

--Assignment 2, Sum of squares
sumsq :: Integer -> Integer
sumsq 0 = 0
sumsq n | n>0 = n*n + (sumsq (n-1))

--Assignment 3,The Towers of Hanoi
--The number of moves needed to move n rings is
--2 * "the number of moves it took to move n-1 rings" + 1.
--It takes 1023 moves to move 10 rings
hanoi :: Integer -> Integer
hanoi 1 = 1
hanoi n = 2 * (hanoi(n-1)) + 1

hanoir :: Integer -> Integer
hanoir 0 = 0                   --Om det ej finns något att flytta finns det inget att flytta
hanoir n = m + 1 + m           --Flyttar alla block förutom den understa, därefter den understa, och sen flyttar den tillbaka alla på den understa
 where 
     m = hanoir (n-1)
 

--wutwut

--Assignment 4, Fibonacci Numbers
fibonacci :: Integer -> Integer
fibonacci 0 = 1
fibonacci 1 = 1
fibonacci n = fibonacci (n-1) + fibonacci (n-2)

fibAux :: Integer -> Integer -> Integer -> Integer
fibAux 0 a b = a + b

-- fibAux i (fibonacci n) (fibonacci ((n+1)) == fibonacci (n+i)

--Assignment 5, Factors

smallestFactor :: Integer -> Integer
smallestFactor n = undefined

nextFactor :: Integer -> Integer -> Integer
nextFactor n k = head [f | f <- [1..n], mod n f == 0, f > k]

nextFactor1 :: Integer -> Integer -> Integer -> Integer
nextFactor1 n k s | mod n s == 0, s > k = s
                  | otherwise = nextFactor1 n k (s+1)

--Assignment 6, Defining Types

data Month = January | February | March | April | May | June | July 
            | August | September | Oktober | November | December
       deriving (Eq, Show)

daysInMonth :: Month -> Integer -> Integer
daysInMonth m y | mod y 4 == 0 && m == February = 29
daysInMonth m _ | m == February                 = 28
                | m == April                    = 30
                | m == June                     = 30
                | m == September                = 30
                | m == November                 = 30
                | otherwise                     = 31

data Date = Date {year::Integer, month:: Month, day::Integer}
      deriving (Eq, Show)

validDate :: Date -> Bool
validDate (Date y m d) = d <= (daysInMonth m y)

--Assignment 7, Replication

repli :: Integer -> String -> String
repli 0 str = ""
repli x str = str ++ repli (x-1) str

--Assignment 8, Multiplying list elements

multiply :: Num a => [a] -> a
multiply [] = 1
multiply (x:xs) = x * multiply xs






