import Data.List
import Data.Maybe
import Test.QuickCheck

--Week 6


-------------------------------------------------------------------------------
--Assignment 2
-------------------------------------------------------------------------------

--A
prop_diff :: Expr -> Bool
prop_diff (Num n) = length(vars(diff (Num n) "x")) == length(vars (Num n))
prop_diff e = length (vars (diff e "x")) <= length (vars e)

prop_diff' e = and [y `elem` xs | y <- ys]
  where
    xs = vars e
    ys = vars (diff e "x")

prop_diff'' :: Expr -> Bool
prop_diff'' (Num n) = length(vars(diff (Num n) "x")) == length(vars (Num n))
prop_diff'' e = length (vars (diff e "x")) >= length (vars e)

--B

simplify :: Expr -> Expr
simplify e | null (vars e) = Num (eval [] e)
simplify e = e

simplify' :: Expr -> Expr
simplify' (Var x) = (Var x)
simplify' (Add a b) | not(null (vars a)) && not(null (vars b)) = (Add a b)
simplify' e = e

prop_SimplifyCorrect e (Env env) = eval env e == eval env (simplify e)

data Polynom = (PAdd Expr Expr)
--x^2+x+1

-----------------------------------------------------------------------

data Env = Env [(Name,Integer)]
 deriving ( Show )

instance Arbitrary Env where
  arbitrary =
    do x <- arbitrary
       y <- arbitrary
       z <- arbitrary
       return (Env [("x",x),("y",y),("z",z)])

-----------------------------------------------------------------------




-------------------------------------------------------------------------------
--Needed for Assignment 2

-----------------------------------------------------------------------

data Expr
  = Num Integer
  | Add Expr Expr
  | Mul Expr Expr
  | Var Name
 deriving ( Eq )

type Name = String

-----------------------------------------------------------------------

showExpr :: Expr -> String
showExpr (Num n)   = show n
showExpr (Add a b) = showExpr a ++ "+" ++ showExpr b
showExpr (Mul a b) = showFactor a ++ "*" ++ showFactor b
showExpr (Var x)   = x

showFactor :: Expr -> String
showFactor (Add a b) = "(" ++ showExpr (Add a b) ++")"
showFactor e         = showExpr e

instance Show Expr where
  show = showExpr

-----------------------------------------------------------------------

eval :: [(Name,Integer)] -> Expr -> Integer
eval env (Num n)   = n
eval env (Add a b) = eval env a + eval env b
eval env (Mul a b) = eval env a * eval env b
eval env (Var x)   = fromJust (lookup x env)

-----------------------------------------------------------------------

instance Arbitrary Expr where
  arbitrary = sized arbExpr

arbExpr :: Int -> Gen Expr
arbExpr s =
  frequency [ (1, do n <- arbitrary
                     return (Num n))
            , (s, do a <- arbExpr s'
                     b <- arbExpr s'
                     return (Add a b))
            , (s, do a <- arbExpr s'
                     b <- arbExpr s'
                     return (Mul a b))
            , (1, do x <- elements ["x","y","z"]
                     return (Var x))
            ]
 where
  s' = s `div` 2

-----------------------------------------------------------------------

vars :: Expr -> [Name]
vars (Num n)   = []
vars (Add a b) = vars a `union` vars b
vars (Mul a b) = vars a `union` vars b
vars (Var y)   = [y]

-----------------------------------------------------------------------

--Derivata!
diff :: Expr -> Name -> Expr
diff (Num n)   x = Num 0
diff (Add a b) x = Add (diff a x) (diff b x)
diff (Mul a b) x = Add (Mul a (diff b x)) (Mul b (diff a x))
diff (Var y)   x
  | x == y       = Num 1
  | otherwise    = Num 0

-----------------------------------------------------------------------

isNum :: Expr -> Bool
isNum (Num _) = True
isNum _       = False



prop_SimplifyNoJunk e =
  noJunk (simplify e)
 where
  -- just an example, can be extended at will!
  noJunk (Add a b) = not (isNum a && isNum b)
                  && noJunk a && noJunk b
  noJunk (Mul a b) = not (isNum a && isNum b)
                  && noJunk a && noJunk b
  noJunk _         = True

-----------------------------------------------------------------------

