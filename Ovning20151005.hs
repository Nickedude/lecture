--Exercise week 5
import Test.QuickCheck
import System.Random
import Test.QuickCheck.Gen

--Assignment 2.B

--map (+3) xs
b1 = [x+3 | x <- xs]

prop_b1 :: [Int] -> Bool
prop_b1 as = [a+3 | a <- as] == (map (+3) as)

--filter (>7) xs

b2 = [x | x <- xs, x > 7]
prop_b2 :: [Int] -> Bool
prop_b2 as = [x | x <- xs, x > 7] == (filter (>7) xs)

--concat (map (\x -> map (\y -> (x,y)) ys) xs)
--där (\y -> (x,y)) är en funktion som tar ett y och parar ihop med ett x
--där (\x -> map (....) xs är en funktion som tar en lista ys och kör (...) på
--där (map (........) xs) tar en lista xs och kör (......) på 

b3 = [(x,y) | x <- xs, y <- ys]
prop_b3 :: [Int] -> [Int] -> Bool
prop_b3 xs ys = (concat (map (\x -> map (\y -> (x,y)) ys) xs)) == [(x,y) | x <- xs, y <- ys]

xs = [1,2,3,4,5]
ys = [6,7,8,9,10]
xys = [(1,2),(3,4),(5,6)]


--filter (>3) (map  (\(x,y) -> x+y) xys)
b4 = [ x+y | (x,y) <- xys, (x+y) > 3]
prop_b4 :: [(Int,Int)] -> Bool
prop_b4 xys = [ x+y | (x,y) <- xys, (x+y) > 3] == (filter (>3) (map  (\(x,y) -> x+y) xys))

----------------------------------------------------------------------------
--Assignment 3
----------------------------------------------------------------------------

listOfLength :: Integer -> Gen a -> Gen [a]
listOfLength n g = sequence [ g | j <- [1..n]]

listOfPairs :: Gen a -> (Gen ([a],[a]))
listOfPairs gen = do
                  t <- choose (1,100)
                  xs  <- (listOfLength t gen)
                  ys  <- (listOfLength t gen)
                  return (xs, ys)


prop_c1 :: Eq a => Eq b => [(a,b)] -> Bool
prop_c1 as = zip xs ys == as
  where
    xs = fst (unzip as)
    ys = snd (unzip as)

prop_c2 :: Eq a => Eq b => [a] -> [b] -> Property
prop_c2 as bs = (length as == length bs) ==> 
  xs == as && ys == bs
  where
    xs = fst (unzip(zip as bs))
    ys = snd (unzip(zip as bs))

--prop_c3 :: [Int] -> [Int] -> Bool

data TwoSameLengthLists a = SameLength [a] [a]
  deriving (Eq, Show)

instance Arbitrary a => Arbitrary (TwoSameLengthLists a) where
  arbitrary = do
              (xs,ys) <- listOfPairs arbitrary
              return(SameLength xs ys)


--Needed for generate
-- QuickCheck >=2.7 includes the function
--
--     generate :: Gen a -> IO a
--
-- which can be used to run a QuickCheck generator as an IO instruction.
--
-- But the Chalmers computers have QuickCheck 2.5 by default. This module shows
-- how to define generate in QuickCheck <2.7.

generate :: Gen a -> IO a
generate g = do
    seed <- newStdGen
    return (unGen g seed 10)

test = do
    a <- generate (arbitrary :: Gen Int)
    b <- generate (arbitrary :: Gen Int)
    c <- generate (arbitrary :: Gen Int)
    sequence_ $ map print [a,b,c]


evenInteger :: Gen Integer
evenInteger = do
    n <- arbitrary
    return (2*n)

----------------------------------------------------------------------------------------

--Week 6

--Assignment 1

data Expr = Num Int | Add Expr Expr | Mul Expr Expr | Sub Expr Expr | Div Expr Expr 

eval :: Expr -> Int
eval (Num n)         = n
eval (Add a b)       = eval a + eval b
eval (Mul a b)       = eval a * eval b
eval (Sub a b)       = eval a - eval b
eval (Div a (Num 0)) = error "Cannot divide by zero!"
eval (Div a b)       = eval a `div` eval b 

eval' :: Expr -> Maybe Int
eval' (Num a) = Just a

eval' (Add a b) = eval' a `add` eval' b
  where
   Nothing `add` _ = Nothing
   _ `add` Nothing = Nothing
   Just x `add` Just y = Just (x+y)

eval' (Div a b) = eval' a `dvv` eval' b
  where
    _ `dvv` Nothing = Nothing
    Nothing `dvv` _ = Nothing
    _ `dvv` Just 0 = Nothing
    Just x `dvv` Just y = Just (div x y) 

showExpr :: Expr -> String
showExpr (Num n) = show n 
showExpr (Add a b) = showExpr a ++ "+" ++ showExpr b
showExpr (Mul a b) = showFactor a ++ "*" ++ showFactor b
showExpr (Sub a b) = showExpr a ++ "-" ++ showExpr b
showExpr (Div a b) = showFactor a ++ "/" ++ showFactor b

showFactor :: Expr -> String
showFactor (Add a b) = "(" ++ showExpr (Add a b) ++ ")"
showFactor (Sub a b) = "(" ++ showExpr (Sub a b) ++ ")"
showFactor e         = showExpr e 


instance Show Expr where
  show = showExpr

size :: Expr -> Int
size (Add a b) = 1 + (size a) + (size b)
size (Mul a b) = 1 + (size a) + (size b)
size (Num a)   = 0


data Tree = Leaf Int | Split Tree Tree
  deriving (Eq, Show)

collapse :: Tree -> [Int]
collapse (Leaf f) = [f]
--collapse (Split s (Leaf f)) = collapse (Leaf f) ++ collapse s
collapse (Split r l) = collapse r ++ collapse l


mirror :: Tree -> Tree
mirror (Leaf f) = (Leaf f) 
mirror (Split a b) = (Split (mirror b) (mirror a)) 

prop_mirrorTwice :: Tree -> Bool
prop_mirrorTwice t = t == (mirror(mirror t))

instance Arbitrary Tree where
  arbitrary = treeGen

treeGen :: Gen Tree
treeGen = frequency
          [(7, do t <- choose (0,9)
                  return (Leaf t))
           ,(6, do t1 <- treeGen
                   t2 <- treeGen 
                   return (Split t1 t2))
          ]

prop_mirColRev :: Tree -> Bool
prop_mirColRev t = (reverse (collapse t)) == (collapse (mirror t))




