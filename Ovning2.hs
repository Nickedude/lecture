import Data.List
import Test.QuickCheck

--Assignment 2
--Måste finnas ett basfall med [], inget av de andra fallen kan
--matchas mot en tom lista. Jag skulle kunna ta bort det andra fallet
--och ändra [] till True. Men det är fel filosofiskt. :-)
sorted :: Ord a => [a] -> Bool
sorted [] = True          
sorted (x:[]) = True
sorted (x:xs) = x <= next xs && sorted xs

--gives the first element in a list, same as head
next :: [a] -> a
next (x:xs) = x

--inserts an element at its correct (sorted) place
insert' :: Ord a => a -> [a] -> [a]
insert' a []              = [a]
insert' a (b:bs) | a <= b = [a] ++ (b:bs)
                 | a > b  = [b] ++ (insert' a bs)

--property to check that the result of insert is sorted
prop_insert :: Ord a => a -> [a] -> Property
prop_insert x xs = sorted xs ==> sorted (insert' x xs)

--isort sorts the list
isort:: Ord a => [a] -> [a]
isort [] = []
isort (a:as) | sorted (a:as) = (a:as)
             | otherwise =  isort(sorthelp a as [])
     
     where 
        sorthelp a [] cs       = insert' a cs
        sorthelp a (b:bs) cs   = sorthelp b bs (insert' a cs)

--An alternative to my isort, since mine was a bit big
isort':: Ord a => [a] -> [a]
isort' [] = []
isort' (x:xs) = insert' x (isort xs)

--We're not interested in empty lists
prop_isort :: Ord a => [a] -> Property
prop_isort list = list /= [] ==> sorted(isort list) && isort list /= []

prop_Sorted :: [Integer] -> Bool
prop_Sorted xs = sorted(isort xs)

--Assignment 7

--Checks if x is a part of a list
--We need Eq in order to be able to compare the a's
--"State it's polymorphic type"
--It can assume any tape that's comparable. Ints, strings.whatever.
occursIn :: (Eq a) => a -> [a] -> Bool
occursIn x xs =  or [ p == x | p <- xs]

prop_occursln :: Int -> [Int] -> Property
prop_occursln x xs = (xs /= [] && elem x xs) ==> occursIn x xs == True

alloccurln :: (Eq a) => [a] -> [a] -> Bool
alloccurln xs ys = (nub xs) == [ a | a <- (nub xs), b <- (nub ys), a==b]

alloccurIn' :: (Eq a) => [a] -> [a] -> Bool
alloccurIn' xs ys = and [elem x ys | x <- xs]

--Facit tycker att de är lika sålänge varje element
--finns i den andra mängden. Dubletter spelar ej roll. 
sameElement :: (Eq a) => [a] -> [a] -> Bool
sameElement xs ys = (alloccurIn' xs ys) && (alloccurIn' ys xs)

alloccurln' :: (Eq a) => [a] -> [a] -> [a]
alloccurln' xs ys = [ a | a <- xs, elem a ys == True]

numOccurrences :: (Eq a) => a -> [a] -> Int
numOccurrences x xs = length [ a | a <- xs, a == x]


bag :: (Ord a) => [a] -> [(a,Int)]
bag xs = nub [(a, (numOccurrences a xs)) | a <- xs]

--Assignment 9

--Alla möjliga par (a,b), cartekesisk produkt. 
pairs :: [a] -> [b] -> [(a,b)]
pairs xs ys = [(x,y) | x <- xs, y <- ys]

triad100 :: [(Integer, Integer, Integer)]
triad100 = [(a,b,c) | a <- [1..100], b <- [1..100], c <- [1..100], 
            a^2 + b^2 == c^2, a <= b, b <= c, c <= 100]

--Assignment 1, Permutations
ispermutation :: Eq a => [a] -> [a] -> Bool
ispermutation (a:[]) bs = [a] == bs
ispermutation (a:as) bs | a `elem` bs = ispermutation as (check a bs)
                        | otherwise = False

check :: Eq a => a -> [a] -> [a]
check a [] = []
check a (b:bs) | a == b = bs
               | otherwise = [b] ++ (check a bs)
--Assignment 3, Avoiding Duplicates
duplicates :: Eq  a => [a] -> Bool
duplicates [] = False
duplicates (a:as) | a `elem` as = True
                  | otherwise = duplicates as

removeduplicates :: Eq a => [a] -> [a]
removeduplicates [] = []
removeduplicates (a:as) | a `elem` as = removeduplicates as
                        | otherwise = [a] ++ removeduplicates as

prop_duplicatsRemoved :: [Integer] -> Bool
prop_duplicatsRemoved xs = not (duplicates (removeduplicates xs))
