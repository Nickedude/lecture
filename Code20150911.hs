import Data.Char
sumSquares :: Integer -> Integer
sumSquares n = sum [i*i | i <- [1..n]]
--Ungefär som en mängd definieras!
--Funkar ungefär som en loop, derp.
--Det kan verka som att den här funktionen
--tar upp väldigt mycket minne, men det gör den
--inte pga .....

--När det står Folal t=> t a, tänk att t är listklammrar

allCAPS :: String -> String
allCAPS str = [toUpper c | c <- str]
--Strängar är listor av chars
--c har typen character
--Där str står finns alltid en lista och där c står
--finns alltid ett element i listan

takeWords :: Int -> String -> String
takeWords n str = unwords $ take n $ words str

--Int pga take
--Tar de n första orden ur en sträng (mellanslag markerar varje ord)
--och returnerar en sträng
--Word str separerar i ord, take tar antalet ord och unwords slår
--ihop dom till en lista.
--Istället för parenteser kan man använda ett $ tecken
--Betyder en parentes ifrån $ till slutet

capitalize :: String -> String
capitalize str = unwords [cap w | w <- words str]
--När _ används i ett uttryck så fungerar det som undefined
--Vi sätter in en mening. Words str delar upp i ord. För varje
--ord w körs cap. På den listan med w som skapas när cap körts så 
--kör vi unword för att slå ihop det till en sträng.

   where
      --När vi skriver såhär funkar/syns bara cap för capitalize
      cap :: String -> String
      cap (c:str) = toUpper c : str
      --Kör toUpper på c och returnerar c+str

--Det vi har gjort hittills är att manipulera ALLA element
--Om man bara vill ändra några då?

keepPositives :: [Integer] -> [Integer]
keepPositives as = [a | a <- as, a > 0]
--Efter "," kan man lägga in ett krav

divisors :: Integer -> [Integer]
divisors n = [f | f <-[1..n], n `mod` f == 0]
--Mod kollar resten vid heltals division, om resten
--är noll så är det en faktor. 
--Man skulle kunna skriva mod n f, men med `` ses
--den som en binär operator

isPrime :: Integer -> Bool
isPrime n = length (divisors n) == 2
--Vi kollar så att talet endast är delbart
--med 1 och sig självt, stämmer det så är det ett primtal.

--TITTA PÅ KODEN PÅ HEMSIDAN!
--[ a+b | a <- [1,2,3], b <- [4,5,6]]
--Vi får alla kombinationer, 1+4, 1+5, 1+6, 2+4 osv
--[ (a,b) | a <- [1,2,3], b <- [4,5,6]]
--Typ cartekesisk produkt

dices :: Integer -> [(Integer,Integer)]
dices n = [(d1,d2) | d1 <- [1..6], d2 <- [1..6], d1+d2 == n]
--Resultatet kommer att bli ett par, (Integer,Integer)
--Vi kan ha olika typer i par, ex (True,"Apa")
--d1+d2 == n kallas för ett filter, vi kan ha flera stycken. 

check_max :: Integer -> Bool
check_max n = and [max a b >= a && max a b >= b | a <- [0..n], b <- [0..n]]
--and testar så att allt i listan är True. 

--TITTA PÅ ETT TILL EXEMPEL PÅ HEMSIDAN

--Lite ifrån förra föreläsningen som vi ej hann med:
summan :: Num a => [a] -> a
summan [] = 0
summan (x:xs) = x + summan xs

--Vi ska definiera funktionen ++ själva
--Det finns ingen anledningen att rekursionen 
--ska gå över den andra listan. Istället flyttar vi
--element för element ifrån första listan till den andra

(+++) :: [a] -> [a] -> [a]
[]     +++ bs = bs --Vi matchar bara mot [] men lägger ej med den
(a:as) +++ bs = a : (as +++ bs)

--Eftersom rekursionen går över endast det första argumentet så 
--är det mer effektivt att köra tex [1] +++ [1..100] en tvärtom

rev :: [a] -> [a]
rev [] = []
rev (a:as) = rev as ++ [a]
--Vi lägger det första elementet sist

--Evaluering:
--rev (1: (2: (3 :[]))) = [1,2,3]
-- = (rev [2,3,[]]) ++ [1]
-- = ((rev [3[]]) ++ [2]) ++ [1]
-- = ((rev [] ++ [3]) ++ [2]) ++ [1]
-- = ((([] ++ [3]) ++ [2]) ++ [1])
-- Totalt fyra rekursioner! Problemet är att den här definitionen
--av revers den har kvadratisk komplexitet. Först kör vi 0 +3, sen
-- 0+3+2, sen 0+3+2+1

-- Vi gör en snabbare version!
rev2 :: [a] -> [a]
rev2 as = revHelp as []
   where
      revHelp []     korg = korg
      revHelp (a:as) korg = revHelp as (a:korg)
      --Vi får in två argument. Vi plockar bort a ifrån det första
      --argumentet och lägger in den i korgen. Två element byter plats.
      --Det går snabbare att lägga till element i början av listan. 

--Evaluering:
-- rev2 [1,2,3,[]]
-- =revHelp [1,2,3,[]] []
-- =revHelp [2,3,[]] [1]
-- =revHelp [3,[]] [2,1]
-- =revHelp [[]] [3,2,1]
-- =revHelp [] [3,2,1]
-- = [3,2,1]

































