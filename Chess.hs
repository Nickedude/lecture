module Chess where

import Test.QuickCheck

--A piece can either be black or white
data Colour = Black | White

--Types for pieces
data PieceType = Pawn | Rook | Knight | Bishop | Queen | King

    
--The different types of pieces
data Piece = Piece {colour :: Colour, piece :: PieceType}
instance Show Piece where 
    show (Piece White King)    = "♔"
    show (Piece White Queen)   = "♕"
    show (Piece White Rook)    = "♖"
    show (Piece White Bishop)  = "♗"
    show (Piece White Knight)  = "♘"
    show (Piece White Pawn)    = "♙"
    show (Piece Black King)    = "♚"
    show (Piece Black Queen)   = "♛"
    show (Piece Black Rook)    = "♜"
    show (Piece Black Bishop)  = "♝"
    show (Piece Black Knight)  = "♞"
    show (Piece Black Pawn)    = "♟"


--A Board consists of a list of rows, each row consists of either a piece or nothing  
data Board = Board [[Maybe Piece]]

--Creates a blank board
blankBoard :: Board
blankBoard = Board[ [ Nothing | sqr <-[1..8]] | r <- [1..8]]

--Function for showing the board
showBoard :: Board -> IO ()
showBoard brd = do putStrLn (unlines (map interpRow (rows brd)))


--Function for interpreting a piece
interpRow :: [Maybe Piece] -> String
interpRow []                = "\n"
interpRow (Nothing:pcs)     = "[ ] " ++ interpRow pcs
interpRow (Just p:pcs)      = "["++show p++"] " ++ interpRow pcs


--Function for getting the rows of a board
rows :: Board -> [[Maybe Piece]]
rows (Board xs) = xs

--Creates a setup of pieces for a player
setup :: Board
setup = Board (reverse ((reverse white)++(drop 2(reverse (black++(drop 2 (rows blank)))))))
-- Board $ map (\x -> ((!!=)  (rows xs) (x,2))) (setupPlayer White)  


wb = zip (white ++ black) [0,1,6,7]
white = (setupPlayer White)
black = (setupPlayer Black)
blank = blankBoard

  
setupPlayer :: Colour -> [[Maybe Piece]]
setupPlayer Black = [ [ Just (Piece Black x) | x <- xs] ,[ Just (Piece Black Pawn) | p <- [1..8]  ] ]
setupPlayer White = [ [ Just (Piece White Pawn) | p <- [1..8]  ], reverse [ Just (Piece White x) | x <- xs]]  
xs = [Rook, Knight, Bishop, King, Queen, Bishop, Knight, Rook]


(!!=) :: [a] -> (a,Int) -> [a]
(!!=) (x:as) (a,0) = a:as
(!!=) (x:as) (a,n) = x:((!!=) as (a,(n-1)))

























