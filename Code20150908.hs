
--A list of integers
--data ListOfInt = Empty | Add Integer ListOfInt
--list1 :: List Integer
--list1 = Add 1 (Add 2 (Add 3 Empty))

--Alist of floats 
--data ListOfFloat = Empty' | Add' Float ListOfFloat
--list2 = Add' 2.3 (Add' 4.5 (Add' 6.6 Empty))

--() Pga listan tar ett arg
data List a = Empty | Add a (List a)

--list3 :: Float
--list3 = Add 2.4 (Add 4.3 Empty)
{-
data FI = F Float | I Integer
list4 :: FI
list4 = Add (F 2.3) --Add (I 4) Empty

Man skulle kunna skriva 
type Hand = List Card
-}

--Polymorf funktion då t kan bytas ut mot massa andra typer
tom :: List t -> Bool
tom Empty = True
tom _     = False

--Här kan vi som sagt byta ut t mot vad som helst
forst :: List t -> t
forst Empty      = error "forst: empty list"
forst (Add a as) = a

sist :: List t -> t
sist Empty         = error "sist: empty list"
sist (Add a Empty) = a
sist (Add a as)    = sist as

size' :: List a -> Integer
size' Empty      = 0
size' (Add a as) = 1 + size' as

size :: [a] -> Integer
size [] = 0
size (a:as) = 1 + size as

--Flera krav: (Num a, Eq a)
--Skriver man ej typ blir Integer default
summan :: Num a => [a] -> a
summan [] = 0
summan (x:xs) = x + summan xs


