import Data.List

--Lecture 20150918

--[f a | a <- as]
--a har typ a, as har typ [as] och f a har typen som f returnerar
--, till exempel b. 

--Hittills har vi bara haft funktioner som beräknar stuff. 
--Idag ska vi kolla på IO, dvs saker programmet ska göra. 

--writeFile :: FilePath -> String -> IO ()
--Det här är en funktion med två argument.
--Den tar en filplats och det som ska skrivas, retur IO ().
--FilePaths typ är String (samma sak)

--Main> writeFile "test.txt" "En fil med lite jävla spam"
--Skriver ej ut resultat, men en fil har skapats. 

--Funktioner i Haskell är rena matematiska funktioner!
--Funktioner får aldrig göra saker. Hur kan då writeFile
--göra någonting? Det är tack vare IO. IO är en instruktion.
--För en viss FilePath och en viss String får man alltid 
--samma instruktion. writeFile är en instrukion!

--WriteFile gör ingent jobb, den skapar en instruktion
--som ghci följer.
writeTwoFiles :: FilePath -> String -> IO ()
writeTwoFiles f content = do writeFile (f ++"1") content
                             writeFile (f ++"2") content

--Readfile returnerar ej en sträng, utan en IO String.
--Dvs instruktioner ang en sträng. Vi får dock lov att
--fånga resultatet från readFile. Str får användas i
--det aktuella do blocket. Str har typen string, ej IO string.
copyFile :: FilePath -> FilePath -> IO ()
copyFile file1 file2 = do str <- readFile file1 
                          writeFile file2 str

catFiles :: FilePath -> FilePath -> IO ()
catFiles file1 file2 = do str1 <- readFile file1
                          str2 <- readFile file2
                          putStrLn str1
                          putStrLn str2

--Det är ghci som tolkar instruktionerna och utför jobbet.                          

--str1 eller 2 är bara namn på resultatet, ej variabel. 
--Sammansatta instruktioner - använd do notation. 

--putStr :: String -> IO () 	-- Defined in `System.IO'
--putStr tar en sträng och returnerar en instruktion
--för att skriva ut strängen. 
--I putStr eller putStrnkan du använda ex \n.

--En instruktion är resultatet av en funktion.
--En instruktion kan hanteras som data. Det kan
--dupliceras, kopieras osv. Nedan skickar vi med
--instruktioner som ett argument och får tillbaka
--instruktioner som ett resultat. 

doTwice :: IO () -> IO ()
doTwice instr = do 
	instr
	instr

doTwice' :: IO a -> IO (a,a)
doTwice' instr = do 
	a <- instr
	b <- instr
	return (a,b) --Måste vara av samma typ som i def. 
--Do notationen kräver att det står en
--instruktion på varje rad. 

--return :: Monad m => a -> m a
--Vi kan tänka att return tar ett
--a och returnerar en instruktion av a(?)

--Om man får lov att göra ngt 2 ggr så får man
--även göra det 0 ggr. Nedan returnerar vi bara
--typen Unit, dvs ingenting. 
dont :: IO a -> IO ()
dont instr = return ()

--Main> dont (writeFile "test2.txt" "hej")
--Bara för att det står writeFile så händer
--det inte någonting. Instruktionen har skapats
--men vi väljer att kasta bort den. 

--Vi kan mönstermatcha på en lista av instruktioner.
--Nedan så kör vi bara den andra instruktionen. 
second :: [IO a] -> IO a
second (i:i2:is) = i2

sortFile :: FilePath -> FilePath -> IO ()
sortFile file1 file2 = do str <- readFile file1
                          writeFile file2 (myFun str)
                          --file2 $unlines $sort $lines str
                          --Vi kan ersätta med en ren funktion

--Vi jobbar hellre med rena funktioner än IO. Det är 
--Det är enklare att jobba med resultatet av en ren
-- funkltion. Det är faktiskt även eklare att testa rena funktioner
--, tex. Vi kan även parallellisera funktioner lätt, en IO funktion är 
--svårare att parallellisera. 
--Vi ska försöka minimera användningen av IO så mycket som möjligt. 

myFun :: String -> String
myFun str = unlines (sort (lines str))

--Vi kan även skriva funktioner som ligger och kör hela tiden. 
--My get line har två instruktioner. En som hämtar s och en  if sats
--som säger att om c är en radbrytning så slutar vi, annars ska den
--hämta resten av listan och returna det hela. 
--Skulle kunna ses ungefär som en while loop. 
myGetLine :: IO String
myGetLine = do c <- getChar
               if c == '\n' 
               	then return ""
               	else do 
               		line <- myGetLine
               		return (c:line)
                --Vi kan använda if för att välja instruktioner
               	--Här räknas det indenterade efter if som innehåll
               	--till if satsen
               --Här ute kan vi ge nästa instruktion. 

--Lista med instruktioner som utförs
mySequence_ :: [IO ()] -> IO ()
mySequence_ []     = return ()
mySequence_ (i:is) = do i
                        mySequence_ is
                        --Vi utför den första instruktionen
                        --Därefter gör vi resten av instruktionerna.

--Lista med resultatet av instruktioner
mySequence :: [IO a] -> IO [a]
mySequence []     =return []
mySequence (i:is) = do a <- i
                       as <- (mySequence is)
                       return (a:as)
                       --Vi kör den första instr sen resten.
                       --Returnerar alla resultaten. 

writeFiles :: FilePath -> [String] -> IO ()
writeFiles file strs = do mySequence_ [writeFile (file ++ show i) str | (str,i) <- zip strs [1..]]

--zip tar 2 listor och parar ihop dom. Den numrerar!
--Den slutar när någon av listorna tar slutar

main = do putStrLn "Which file name?"
          file <- myGetLine
          putStrLn "What content?"
          content <- myGetLine
          writeFile file content

--./Code20150918 för att köra 
