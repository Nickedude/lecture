--Code from lecture 20150915

import Test.QuickCheck
import Data.Char

--Ladda ej upp kod från labbar offentligt (github?)

--Samma som ++
(+++) :: [a] -> [a] -> [a]
[] +++ bs = bs
(a:as) +++ bs = a : (as +++ bs)

--Testar associativiteten hos append
--Bra att fundera över olika generella egenskaper
--hos funktioner man skriver
--Eq a säger att a måste stödja =
--prop_appendAssoc :: Eq a => [a] -> [a] -> [a] -> Bool
--() kallas unit. ()::() Unit har typen unit
--Unit är default a, då kan testet gå igenom även om vi har en bugg
--Unit är inte bra att testa med då den bara har ett värde, num bättre

prop_appendAssoc :: (Eq a, Num a) => [a] -> [a] -> [a] -> Bool
prop_appendAssoc as bs cs =
     as +++ (bs +++ cs) == (as +++ bs) +++ cs

--Om vi har en sträng "145" så vet vi att det =
--1 * 100 + 4*10 + 5*1 = 5*1 + 10(1*10 + 4*1)
--Parentesen = value "14". Vi kan ta 5 + 10 * 14
--Blir problem när vi börjar med den mest signifikanta siffran
valueHelp :: String -> Int
valueHelp ""     = 0
valueHelp (a:as) = digitToInt a + 10 * valueHelp as

value :: String -> Int
value str = valueHelp (reverse str)

prop_value :: String -> Property
prop_value str = (s /= "" && head s /= '0') ==> show (value s) == s
     where
        s = [d|d<-str, isDigit d]

prop_value2 :: Int -> Property
prop_value2 n = n >= 0 ==> n == value (show n)

--Dålig idé, testar otherwise flera ggr
prop_value' :: String -> Property
prop_value'  str | s /= "" && head s /= '0' = 
              collect "interesting" (show (value s) == s)
                 | otherwise = collect "uninteresting" True
           where
        s = [d|d<-str, isDigit d]

--collect :: (Show a, Testable prop) => a -> prop -> Property
--prop är en property som quickcheck kan testa

--Arbitrary är en klass som tillhandahåller en metod som
--heter arbitrary. Gen används för att generera värden. 
--Hur vet quickcheck vad den ska slumpa fram?
--Man berättar det via sk instanser. 
--Det är viktigt att man genererar intressanta värden

prop_reverselength ::  [Int] -> Bool
prop_reverselength xs = length (reverse xs) == length xs

--(!!) tar en lista och ett index, returnerar elementet på indexet
         
prop_reverseindex :: [Int] -> Int -> Property
prop_reverseindex xs i = (length xs > i && i >= 0 && length xs > 0)==> (reverse xs !! i) == (xs !! (length xs - i -1))

--Tar de n första elementen av en lista
tak :: Int -> [a] -> [a]
tak n []      = []
tak 0  _      = []
tak n (a:as)  = a: tak (n-1) as

--Om det finns mer element än n så ska längden av tak
--strängen vara lika med n. 
prop_TakLength :: Int -> [Int] -> Property
prop_TakLength n xs =
           (length xs >= n && n >= 0) ==> length(tak n xs) == n

prop_TakLength' :: Int -> [Int] -> Property
prop_TakLength' n xs
           | length xs >= n = n>= 0 ==> length(tak n xs) == n
           | otherwise      = n>= 0 ==> length(tak n xs) == length xs

prop_TakeDrop :: Int -> [Int] -> Bool
prop_TakeDrop n xs = (tak n xs ++ drop n xs) == xs

---------------------------------------------------------------

--Bra att veta om quickcheck

prop_commutative x y = x + y == y + x

prop_associative :: Double -> Double -Double -> Bool
prop_associative x y z = ((x
        where
        x~= y = abs (x-y) < 0.001

prop_reverse xs = reverse (reverse xs) == xs
       where
         _ = xs :: [Int]
--Tvingar xs att vara int


--Quickcheck hittar inte alltid "hörnfallen"
--Chansen att 1000 slumpas fram är liten
--Chansen att små värden slumpas fram är störr
prop_not_1000 :: Int -> Bool
prop_not_1000 x = x /= 1000

--quickCheckWith stdArgs {maxSuccess = 1000}
--Testar 1000 fall








