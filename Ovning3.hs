import Test.QuickCheck
import Data.List

--Assignment 1, Properties of Look Function
look :: Eq a => a -> [(a,b)] -> Maybe b
look x [] = Nothing
look x ((x',y):xys) | x == x' = Just y
                    | otherwise = look x xys

prop_LookNothing:: Eq a => a -> [(a,a)] -> Property
prop_LookNothing a xys = (look a xys == Nothing) ==> notin a xys

notin :: Eq a => a -> [(a,a)] -> Bool
notin a []          = True
notin a ((x,y):xys) = a /= x && (notin a xys) -- a /= y

prop_LookJust :: Eq a => a -> [(a,a)] -> Property
prop_LookJust a abs = (look a abs /= Nothing) ==> isin (a,d) abs
  where d = look a abs

isin :: Eq a => (a,Maybe a) -> [(a,a)] -> Bool
isin (x,Just y) [] = False
isin (x,Just y) ((a,b):abs) | (x,y) == (a,b) = True
                            | otherwise = isin (x, Just y) abs

prop_LookSucceed :: Eq a => a -> [(a,a)] -> Property
prop_LookSucceed k abs = (notin k abs == False) ==> look k abs == Just l
  where l = buddy k abs

--Returns a's buddy
buddy :: Eq a => a -> [(a,a)] -> a
buddy a [] = error "No list entered"
buddy a ((x,y):[]) = y
buddy a ((x,y):xys) | a == x = y
                    | otherwise = buddy a xys

prop_Look :: Eq a => a -> [(a,a)] -> Bool
prop_Look a abs = if notin a abs && look a abs == Nothing
	              then True
	              else if isin (a,d) abs && look a abs/= Nothing
	              then True
	              else False
  where d = look a abs

--Assignment 2, Properties of theprefixOf function
prefixOf :: Eq a => [a] -> [a] -> Bool 
prefixOf [] _          = True
prefixOf _ []          = False
prefixOf (x:xs) (y:ys) = x == y && prefixOf xs ys

prop_prefixOfSelf :: Int -> String -> Bool
prop_prefixOfSelf x str = prefixOf (take x str) str

prop_suffixOfSelf :: Int -> String -> Bool
prop_suffixOfSelf x str = isSuffixOf (reverse (take x (reverse str))) str

prop_prefixOfSelf' :: Eq a => [a] -> [a] ->Property
prop_prefixOfSelf' xs ys = (prefixOf xs ys) 
                   ==> xs == take (length xs) ys

